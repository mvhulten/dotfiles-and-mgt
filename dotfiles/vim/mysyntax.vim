au BufRead,BufNewFile *.jnl set filetype=ferret
au! Syntax ferret source ~/.vim/ferret.vim
